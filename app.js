var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var app = express();

app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({extended: true}));

// Connect to database
mongoose.connect('mongodb://localhost/test', {useNewUrlParser: true})
.catch(error => {console.log("Caught ", error.message); });

var campgrounds = [
  {name: "Salmon Creek", image: "https://mk0theadventuregfnyq.kinstacdn.com/wp-content/uploads/Tips-for-Hiking-and-Camping-in-the-Snow-3.jpg"},
  {name: "Granite Hill", image: "https://www.bluegrass.com/images/telluride/camping/CampLH_02.jpg"},
  {name: "Torni Talu", image: "https://static1.visitestonia.com/images/3103015/Seaside+heated+tent+and+grilling+terrace+-+Torni+Talu+Holiday+Cottages_.JPG"},

  {name: "Salmon Creek", image: "https://mk0theadventuregfnyq.kinstacdn.com/wp-content/uploads/Tips-for-Hiking-and-Camping-in-the-Snow-3.jpg"},
  {name: "Granite Hill", image: "https://www.bluegrass.com/images/telluride/camping/CampLH_02.jpg"},
  {name: "Torni Talu", image: "https://static1.visitestonia.com/images/3103015/Seaside+heated+tent+and+grilling+terrace+-+Torni+Talu+Holiday+Cottages_.JPG"},

  {name: "Salmon Creek", image: "https://mk0theadventuregfnyq.kinstacdn.com/wp-content/uploads/Tips-for-Hiking-and-Camping-in-the-Snow-3.jpg"},
  {name: "Granite Hill", image: "https://www.bluegrass.com/images/telluride/camping/CampLH_02.jpg"},
  {name: "Torni Talu", image: "https://static1.visitestonia.com/images/3103015/Seaside+heated+tent+and+grilling+terrace+-+Torni+Talu+Holiday+Cottages_.JPG"}
]


app.get("/", function(req,res){
  res.render("landing");
});

app.get("/campgrounds", function(req,res){
  res.render("campgrounds", {campgrounds: campgrounds});

});

app.get("/campgrounds/new", function(req,res){
  res.render("new.ejs");
});

app.post("/campgrounds", function(req,res){
  var name = req.body.name;
  var image = req.body.image;
  newCampground = {name: name, image: image};
  campgrounds.push(newCampground);
  res.redirect("/campgrounds");
});

//Set up port
port = process.env.PORT || 3000;

//listen on port 3000
app.listen(port, function(){
  console.log("server listening on port "+port);
});
